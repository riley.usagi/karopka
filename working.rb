require 'rubygems'
require 'active_record'
require 'yaml'
require 'mechanize'

require_relative 'lib/working'

# Загружаем файл настройки соединения с базой данных
dbconfig = YAML.load_file(File.join(__dir__, 'database.yml'))

# Ошибки работы с БД направим в стандартный поток (консоль)
ActiveRecord::Base.logger = Logger.new(STDERR)

# Соединяемся с БД
ActiveRecord::Base.establish_connection(dbconfig['development'])

agent = Mechanize.new

page_number = 1

7.times do
  page = agent.get('http://karopka.ru/catalog/working/?f=-1&p=' + page_number.to_s)

  # Сами объявления
  items = Item.items_links(page)

  # Собираем данные
  Item.get_info(items)
  page_number += 1

  puts '========================= PAGE NUMBER: ' + page_number.to_s + ' ============================='
end
