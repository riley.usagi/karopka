require 'rubygems'
require 'active_record'
require 'yaml'
require 'mechanize'

class Item < ActiveRecord::Base
  def self.items_links(page)
    links = page.links_with(href: /MODEL/).last(18)

    links
  end

  def self.get_info(links)
    agent = Mechanize.new

    links.each do |link|
      # Переходим на страницу конкретного объявления
      item_page = link.click

      images = []

      # Собираем данные со страницы
      title             = item_page.search('h1').text
      photolist         = item_page.search('.ps-full .pic')
      model_description = item_page.search('.model-description').text
      model_info        = modelinfo(item_page.search('.modelinfo').search('tr').search('td').first.text)

      # Создаём директорию из названия товара
      Dir.mkdir('diorama/' + clear(title)) unless File.exist?('diorama/' + clear(title))

      # Сохраняем описание в файл
      desc_file = File.new(('diorama/' + clear(title) + '/' + 'desc.txt'), 'w')

      desc_file.puts("Характеристики:\n")
      desc_file.puts(model_info)

      desc_file.puts("\n")

      desc_file.puts(model_description)
      desc_file.close

      # Перебираем все фото товара и сохраняем в созданную папку
      photolist.each do |photo|
        image      = ('http://karopka.ru' + photo.children.attr('href').value)
        image_name = image.split('/').last
        path = ('diorama/' + clear(title) + '/' + image_name)
        agent.get(image).save(path)
      end

      puts title
    end
  end

  private

  def self.clear(title)
    title.tr('/', '_')
  end

  def self.modelinfo(info)
    info.gsub!('Производитель:', 'Производитель: ')
    info.gsub!('Дата размещения:', 'Дата размещения: ')
    info.gsub!('Раздел галереи:', 'Раздел галереи: ')
    info.gsub!('Тип бронетехники:', 'Тип бронетехники: ')
    info.gsub!('Модель времён ВМВ:', "\nМодель времён ВМВ: ")
    info.gsub!('Год выпуска:', "\nГод выпуска: ")
    info.gsub!('Страна-производитель:', "\nСтрана-производитель: ")
    info.gsub!('Масштаб модели:', "\nМасштаб модели: ")
    info
  end
end
